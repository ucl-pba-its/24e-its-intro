---
hide:
  - footer
---

# Introduktion til IT sikkerhed Efterår 2024

På dette website finder du ugeplaner, dokumenter og links til brug i faget *Introduktion til IT sikkerhed*

Lektionsplanen finder du under [dokumenter](./other_docs/its_intro_lecture_plan.md).  
[Ugeplaner](./weekly/ww_01.md) giver dig en oversigt over fagets indhold, uge for uge.  
Under [øvelser](./exercises/1_intro_opgave_markdown.md) kan du se fagets øvelser. 

# Fagets indhold

Faget omhandler grundlæggende programmering og netværk med henblik på it-sikkerhed.  
Den studerende bliver introduceret til programmering med et programmeringssprog, der ofte anvendes indenfor sikkerhedsprofessionen.  
Den studerende skal have viden, færdigheder og kompetencer i centrale begreber i forhold til it-sikkerhed, i forhold til netværk (grundlæggende begreber som trafik monitorering ved sniffing).  
Yderligere kigges der på sikkerhedsaspekter ved protokollerne.

Faget er på 5 ECTS point.

# Studieordning

Studieordningen er i 2 dele, en national del og en institutionel del.  

Begge dele kan findes i [studiedokumenter på ucl.dk](https://www.ucl.dk/studiedokumenter/it-sikkerhed)

## Læringsmål fra studieordningen

### Viden

Den studerende har viden om og forståelse for:  

- Grundlæggende programmeringsprincipper
- Grundlæggende netværksprotokoller
- Sikkerhedsniveau i de mest anvendte netværksprotokoller

### Færdigheder

Den studerende kan supportere løsning af sikkerhedsarbejde ved at:  

- Anvende primitive datatyper og abstrakte datatyper
- Konstruere simple programmer der bruge SQL databaser
- Konstruere simple programmer der kan bruge netværk
- Konstruere og anvende tools til f.eks. at opsnappe samt filtrere netværkstrafik
- Opsætte et simpelt netværk.
- Mestre forskellige netværksanalyse tools
- Læse andres scripts samt gennemskue og ændre i dem

### Kompetencer

Den studerende kan:   

- Håndtere mindre scripting programmer set ud fra et it-sikkerhedsmæssigt perspektiv

# Eksamen datoer

Se semesterbeskrivelsen [https://esdhweb.ucl.dk/D24-2601256.pdf](https://esdhweb.ucl.dk/D24-2601256.pdf)


