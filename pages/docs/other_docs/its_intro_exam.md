---
title: '24E - Introduktion til IT sikkerhed'
subtitle: 'Eksamen beskrivelse'
filename: '23E_ITS1_INTRO_eksamens_opgave'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2024-04-08
email: 'nisi@ucl.dk'
left-header: \today
right-header: Eksamen beskrivelse
skip-toc: false
semester: 24E
---

# 24E - Introduktion til IT sikkerhed eksamen beskrivelse

# Dokumentets indhold

Dette dokument indeholder praktiske informationer samt spørgsmål til eksamen i faget: _introduktion til IT sikkerhed_.

# Eksamen beskrivelse

Eksamen er beskrevet i den institutionelle del af studieordningen _afsnit 3.2.2, side 16_ [https://esdhweb.ucl.dk/D24-2522389.pdf](https://esdhweb.ucl.dk/D24-2522389.pdf)

Tidsplan for eksamen kan findes på wiseflow.  
Eksamen er med intern censur.

For hvert spørgsmål/emne bør den studerende forberede en præsentation/oplæg på max 10 minutter, det anbefales at vise eksempler på kode og brug af værktøjer.  

Det er **IKKE** tilladt at bruge eksemplerne fra undervisningsmaterialet i uredigeret form.  

Efter den studerendes præsentation stiller eksaminator og censor spørgsmål.  
Oplæg og spørgsmål tager samlet 20 minutter, de sidste 5 minutter er afsat til votering.

# Eksamen spørgsmål/emner

1. Netværk med fokus på OSI, TCP/IP modeller og netværksprotokoller samt (u)sikkerhed i disse.
2. Netværk med fokus på trafikmonitorering og skanning samt hvordan dette kan anvendes i arbejdet med sikkerhed.
3. Programmering med database og fokus på database (u)sikkerhed.
4. Programmering med netværk og fokus på sikkerhed i protokoller.
5. Scripting i bash og powershell med fokus på hvordan det kan anvendes i arbejdet med sikkerhed.

# Eksamen datoer

- Se semesterbeskrivelsen [https://esdhweb.ucl.dk/D24-2601256.pdf](https://esdhweb.ucl.dk/D24-2601256.pdf)
