# Øvelse 12 - Python netværks programmering

## Information

Python har flere biblioteker der gør det forholdsvist simpelt at kommunikere over netværk.  
I denne øvelse får du fingrene i

- `socket` biblioteket til at kommunikere via sockets.
- `urllib` biblioteket der gør det simpelt at arbejde med _Uniform Resource Locators_ [https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_URL](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_URL)
- `BeatifulSoup` til at parse html sider og filtrere på html tags, id, klasser mm.
- `Feedparser` til at parse RSS feeds [http://www.rss-specifications.com/](http://www.rss-specifications.com/)

I nogle af deløvelserne skal du bruge Wireshark, du er selvfølgelig velkommen, til også at wiresharke i de øvrige øvelser.

Det kan måske også være en god øvelse at anvende burpsuite når du arbejder, prøv om du kan få det til at virke :-)

## Instruktioner

1. Fork projektet [https://gitlab.com/npes-py-experiments/network-programs](https://gitlab.com/npes-py-experiments/network-programs) til dit eget gitlab namespace
2. Klon projektet til din computer
3. Lav et virtual environment og installer dependencies som beskrevet i readme.md filen [https://gitlab.com/npes-py-experiments/network-programs#development-usage](https://gitlab.com/npes-py-experiments/network-programs#development-usage)

**socket**

1. Læs om pythons socket bibliotek [https://docs.python.org/3/library/socket.html](https://docs.python.org/3/library/socket.html)
2. Læs koden i `socket_1.py` Hvad gør koden og hvordan bruger den socket biblioteket?
3. Kør koden med default url og observer hvad der sker, du kan også prøve at single steppe filen for at kunne følge med i variabler osv. [https://python.land/creating-python-programs/python-in-vscode](https://python.land/creating-python-programs/python-in-vscode)
4. Åben wireshark (installer det hvis du ikke allerede har det)
5. Kør koden igen med default url, analyser med wireshark og besvar følgende:
    - Hvad er destinations ip ?
    - Hvilke protokoller benyttes ?
    - Hvilken content-type bruges i http header ?
    - Er data krypteret ?
    - Hvilken iso standard handler dokumentet om ?
6. Læs `socket_2.py` og undersøg hvordan linje 38 - 47 kan tælle antal modtagne karakterer
7. Kør `socket_2.py` og prøv at hente forskellige url's der benytter https, hvad modtager du, hvilken http response får du, hvad betyder de og er de krypteret ?
8. Gentag trin 7 og analyser med wireshark for at finde de samme informationer der
9. Åben `socket_3.py`, analyser koden og omskriv den så kun http headers udskrives.

**urllib**

1.  Læs om urllib biblioteket [https://docs.python.org/3/library/urllib.html](https://docs.python.org/3/library/urllib.html)
2.  Åbn `urllib_1.py` og læs koden, hvordan er syntaksen ifht. socket 1-3 programmerne ?
3.  Hvilket datasvar får du retur hvis du prøver at hente via https, f.eks [https://docs.python.org/3/library/urllib.html](https://docs.python.org/3/library/urllib.html)
4.  Kør programmet igen med [https://docs.python.org/3/library/urllib.html](https://docs.python.org/3/library/urllib.html) og analyser i wireshark, besvar følgende:
    - Hvad er destinations ip ?
    - Hvilke protokoller benyttes ?
    - Hvilken content-type bruges i http(s) header ?
    - Er data krypteret ?
    - Hvor mange cipher suites tilbydes i `Client hello` ?
    - Hvilken TLS version og cipher suite bliver valgt i `Server Hello` ?

**BeautifulSoup**

1. Læs om BeatifulSoup biblioteket [https://beautiful-soup-4.readthedocs.io/en/latest/](https://beautiful-soup-4.readthedocs.io/en/latest/)
   - Hvad er formået med biblioteket ?
2. Åbn `urllib_2.py` og analyser koden for at finde ud af hvad den gør.
3. Kør programmet
4. Ret i programmet så det tæller et andet HTML tag
5. Ret i programmet så det bruger BeatifulSoups `.findall()` metode

**Feedparser**

1.  Læs om feedparser biblioteket [https://feedparser.readthedocs.io/en/latest/](https://feedparser.readthedocs.io/en/latest/)
2.  Åbn `rssfeed_parse.py` og analyser koden for at finde ud af hvad den gør.
3.  Linje 28, som formatterer modtaget data, har en meget lang sætning.

    ```py title="rssfeed_parse.py"
    f'# {entry.title}\n**_Author: {entry.author}_**  \nPublished:  {entry.published}  \n**_Summary_**  \n{re.sub(r"[^a-zA-Z0-9]", " ", entry.summary).replace(" adsense ", " ").replace(" lt ", " ").replace(" gt ", " ")}  \n  \nLink to full article:  \n[{entry.link}]({entry.link})\n'
    ```

    Hvad gør den ? ( du kan prøve at erstatte den med `{entry.summary}` for at se forskellen)

4.  Find et passende sikkerheds relateret rss feed, på internettet, du vil parse.
5.  Omskriv `rssfeed_parse.py` til at bruge dit valgte feed, ret i formatteringen, så du får et output i markdown filen, der svarer til:

    ```markdown
    # Headline/title

    **_Author:_**
    **_Summary:_**
    [link_to_full_article](link_to_full_article)
    ```

## Links

- [https://www.markdownguide.org/cheat-sheet](https://www.markdownguide.org/cheat-sheet)
