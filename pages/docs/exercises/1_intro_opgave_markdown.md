# Øvelse 1 - Markdown

## Information

Det du læser lige nu er skrevet som tekstfil og derefter renderet til html med [MKDocs](https://www.mkdocs.org/) og [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/).  
Tekstfilens format er Markdown. Markdown har en meget simpel formaterings syntax og renderes default på mange platforme som f.eks github og Gitlab, udover det bruges Markdown mange andre steder.  

Endelig er markdown, efter lidt tilvænning, virkelig brugbart til at tage noter, både på studiet eller når du laver pentesting etc.

## Instruktioner

1. Læs om markdown og hvad du kan bruge det til [https://www.markdownguide.org/getting-started](https://www.markdownguide.org/getting-started)
2. Kig på markdown syntax i dette cheatsheet [https://www.markdownguide.org/cheat-sheet](https://www.markdownguide.org/cheat-sheet)
3. Undersøg hvordan du kan previewe markdown i din favorit editor.
4. Lav en test markdown fil `.md`  og prøv mulighederne af ved hjælp af cheat sheetet fra punkt 2 (det kan godt være du skal søge lidt ekstra på nettet for at gøre alt nedenstående)
      - Lav 3 forskellige overskrifter
      - Lav et link
      - Indsæt et billede (link til et billede)
      - Lav en 4\*4 tabel med første kolonne venstre justeret, kolonne 2 og 3 centreret og kolonne 4 højre justeret.
      - Lav en unordered list (bullet points) med 3 niveauer
      - Lav en ordered list med 2 niveauer
      - Lav fed tekst og italic tekst. Kan man lave fed italic tekst samtidig ?
      - Lav en code block og angiv det sprog koden er skrevet i.
      - Find selv på mere hvis du har ekstra tid. Kig evt. på bonus herunder.

### Bonus

[https://obsidian.md/](https://obsidian.md/) er ret kool til at holde styr på dine markdown filer lokalt, du kan selvfølgelig gemme din vault remote hvor du ønsker det. Hvem ved, måske bliver det måden du laver notater igennem uddannelsen ?  
[https://joplinapp.org/](https://joplinapp.org/) er også en mulighed som er lidt mindre manuel i forhold til obsidian.

## Links
