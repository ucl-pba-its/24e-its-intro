# Øvelse 17 - Opsætning af opnsense virtuel router

### Information

I denne øvelse skal du opsætte en virtuel opnsense router i VirtualBox

![opnsense_intro_til_itsik1.png](opnsense_intro_til_itsik0.png)  
*Netværksdiagram*

### Instruktioner

1. download **opnsense-its-intro-24.ova** fra itslearning
2. importer ovf filen i VirtualBox
4. åbn indstillinger og bekræft at de 4 netværksinterfaces er konfigureret som nedenstående billede (se også netværks diagram)  
Hvis de ikke er konfigureret så skal du gøre det!
<figure markdown="span">
   ![opnsense_nw_settings_adapter1.png](opnsense_nw_settings_adapter1.png){ width="600" }
   <figcaption>opnsense netværks konfiguration adapter 1</figcaption>
</figure>

<figure markdown="span">
   ![opnsense_nw_settings_adapter2.png](opnsense_nw_settings_adapter2.png){ width="600" }
   <figcaption>opnsense netværks konfiguration adapter 2</figcaption>
</figure>

<figure markdown="span">
   ![opnsense_nw_settings_adapter3.png](opnsense_nw_settings_adapter3.png){ width="600" }
   <figcaption>opnsense netværks konfiguration adapter 3</figcaption>
</figure>

<figure markdown="span">
   ![opnsense_nw_settings_adapter4.png](opnsense_nw_settings_adapter4.png){ width="600" }
   <figcaption>opnsense netværks konfiguration adapter 4</figcaption>
</figure>
6. Kontroller at de enkelte netværk er forbundne ved at pinge fra lubuntu maskinen til kali maskinen og fra kali maskinen til lubuntu maskinen.  
      - Hvis du aldrig har pinget før så start med at læse om hvad og hvordan her:  
      [https://en.wikipedia.org/wiki/Ping_(networking_utility)](https://en.wikipedia.org/wiki/Ping_(networking_utility))  
      - Spørg derefter en fra din gruppe om de vil vise hvordan man gør, alternativt må du meget gerne spørge din underviser :-)    

### Links

...