# Øvelse 18 - Udforskning af opnsense cli og webinterface 

## Information

I denne øvelse skal du oprette forbindelse til opnsense cli og webinterface.  
Jeg kalder opnsense for en router, men i virkeligheden er det meget mere end det, feks. en firewall, DNS proxy, IDS/IPS og meget mere.  

Du kan danne dig et overblik på [https://opnsense.org/](https://opnsense.org/)

### CLI interface

Opnsense Commmand Line Interface (cli) bruges til at administrere opnsense på den maskine opnsense er installeret på.
I daglig tale kaldes det også konsoladgang eller konsolinterface.  

CLI vil typisk være det interface du bruger til at konfigurere routeren første gang. Her vil du konfigurere netværksadresser, netværksstørrelser, DHCP mm. for de enkelte netværksinterfaces (NIC).  

Jeg har allerede konfigureret opnsense maskinen for jer. Den har 4 NIC's som hedder hhv. LAN, OPT1, OPT2 og WAN. Navngivningen er tildelt af opnsense og jeg har valgt at beholde navnene.    

![opnsense_cli_interface.png](opnsense_cli_interface.png)  
_opnsense cli interface_  

WAN er konfigureret til at modtage en IP adresse fra DHCP, det vil i dette tilfælde betyde at virtualbox tildeler en "WAN" adresse som oversættes af virtualbox ved hjælp af NAT.  

![opnsense_nw_settings_adapter1.png](opnsense_nw_settings_adapter1.png){ width="60%" }  
_opnsense virtualbox network settings adapter1 (WAN)_  

LAN, OPT1 og OPT 2 er alle forbundet til interne netværk i virtualbox. På den måde vil det være opnsense der fungerer som router for de 3 interfaces/netværk.  
Jeg har valgt at kalde dem intnet 1 - 3 i virtualbox. Det er ikke så vigtigt hvilket navn de har, så længe de kan identificeres.  

![opnsense_nw_settings_adapter2.png](opnsense_nw_settings_adapter2.png){ width="60%" }  
![opnsense_nw_settings_adapter3.png](opnsense_nw_settings_adapter3.png){ width="60%" }  
![opnsense_nw_settings_adapter4.png](opnsense_nw_settings_adapter4.png){ width="60%" }   
_opnsense virtualbox network settings adapter 2, 3, 4 (LAN, OPT1, OPT2)_  

### Web interface

pr. default er opnsense web interfacet kun tilgængeligt fra interne netværk, altså ikke fra WAN siden. Det vil sige at du skal bruge en maskine som er tilsluttet et internet netværk, altså alle undtaget WAN.  

Inden du kan tilgå web interfacet skal du logg ind i opnsense. Her har jeg valgt at beholde default credentials fordi routeren bruges i et virtuelt miljø som kun jeg har adgang til.  

Det er faktisk dårlig praksis at gøre det selvom det er et test miljø. Det ses alt for mange gange at default **brugernavn:password** ikke ændres når test miljøet migreres til produktion.  
Det er en længere debat som også bør inkludere vigtigheden af at implementere Multi Factor Authentication (MFA) som opnsense selvfølgelig også understøtter.    

Du kan logge ind i opnsense via web med disse credentials: **root:opnsense**  

![opnsense_web_login.png](opnsense_web_login.png){ width="300" }  
_opnsense web login_  

Web interfacet kan være lidt overvældende hvis du ikke har brugt opnsense før. Det er fra web interfacet at det meste af opnsense funktionaliteten konfigureres.  

Det er også fra webinterfacet du kan se logs og statistik over netværkstrafik, firewall og systemhændelser.  


![opnsense_web_dashboard.png](opnsense_web_dashboard.png)  
_opnsense web dashboard_  

![opnsense_intro_til_itsik0.png](opnsense_intro_til_itsik0.png)  
*Netværksdiagram*

## Instruktioner

1. Log på CLI og kontroller at interfaces og netværks adresser stemmer overens med netværksdiagrammet.
    - Noter WAN adressen
    - Hvis noget ikke stemmer overens så fejlsøg (husk at spørge om hjælp)
    - Hvis der er noget du ikke forstår, så spørg om hjælp! 
    - Når du forstår diagram og CLI interface informationen så fortsæt til næste trin.
2. Log på web interfacet og gennemgå dokumentationen for opnsense på [https://docs.opnsense.org/](https://docs.opnsense.org/) samtidig med at i finder tingene på jeres opnsense installation i virtualbox.  
    Besvar følgende og dokumenter på gitlab pages:  
        
    1. Hvilke features tilbyder opnsense? 
    2. Hvordan kan du kontrollere om din opnsense version har kendte sårbarheder? 
    3. Hvad er lobby og hvad kan man gøre derfra? 
    4. Kan man se [netflow](https://en.wikipedia.org/wiki/NetFlow) data direkte i opnsense og i så fald hvor kan man se det? 
    5. Hvad kan man se omkring trafik i `Reporting`? 
    6. Hvor oprettes vlan's og hvilken IEEE standard anvendes? 
    7. Er der konfigureret nogle firewall regler for jeres interfaces (LAN og WAN)? Hvis ja, hvilke?
    8. Hvilke slags VPN understøtter opnsense? 
    9. Hvilket IDS indeholder opnsense?
    10. Hvor kan man installere `os-theme-cicada` i opnsense? 
    11. Hvordan opdaterer man firmware på opnsense?
    12. Hvad er nyeste version af opnsense? 
    13. Er jeres opnsense nyeste version? Hvis ikke så opdater den til nyeste version :-)

## Links

- [opnsense dokumentation](https://docs.opnsense.org/)