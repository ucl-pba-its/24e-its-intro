# Øvelse 13 - Bash script til vm konfigurering

## Information

I stedet for manuelt at opdatere og installere applikationer på en server/maskine/vm kan det være en god ide at gøre dette fra et script.  
Det er anvendt i industrien og hører under begrebet "immutable infrastructure".  
Immutable infrastructure handler om at man aldrig opdaterer softwaren på maskinen manuelt, men blot starter en ny virtuel maskine som har et script der automatisk henter nyeste opdateringer og compiler samt installerer hvad end der skal bruges (ved opstart). Det giver den fordel at man ved f.eks. angreb, nemt blot slukker den gamle webserver og starter en ny.

Eksempler/løsninger kan findes her:

- Øvelse 3 [https://gitlab.com/ucl-pba-its/22s-its1-intro/-/tree/master/docs/examples/vm_deploy_scripts](https://gitlab.com/ucl-pba-its/22s-its1-intro/-/tree/master/docs/examples/vm_deploy_scripts)

## Instruktioner

1. Se denne video om mutable/immutable infrastructure [https://youtu.be/II4PFe9BbmE](https://youtu.be/II4PFe9BbmE)
2. Overvej følgende:
   - Hvordan kan immutable infrastructure bruges i forhold til sikkerhed?
   - Er der ulemper?
   - Hvordan kører man et script ved opstart af en linux maskine?
3. Installer en virtuel maskine i vmware, brug xubuntu 20.04 LTS som image.
   - Xubuntu image download [https://xubuntu.org/download/](https://xubuntu.org/download/)
   - hjælp til at installere en ny vm [https://kb.vmware.com/s/article/1018415](https://kb.vmware.com/s/article/1018415)
4. På din ny installerede vm lav et bash script der ved boot:
   - Checker om maskinen har forbindelse til internet
   - Automatisk opdaterer maskinen (via apt-get update/upgrade).
   - Installerer apache2 og starter webserveren (check at den kører på localhost port 80)
   - Logger installations output i en text fil (her kan [https://linuxize.com/post/linux-tee-command/](https://linuxize.com/post/linux-tee-command/) være en god kandidat)
   - EKSTRA: Få scriptet til at checke om der findes nye opdateringer og kun kører hvis der er.
   - EKSTRA: Udvid scriptet til at acceptere en liste af applikationer som scriptet installerer.
5. Lav en klon af din xubuntu vm og check at scriptet samt apache også kører ved boot på den klonede vm

## Links
