# Øvelse 3 - Gitlab pages til personlig portfolio

## Information

Gitlab giver mulighed for [Continuous Integration, Continuous Delivery and Continuous Deployment (CI/CD)](https://docs.gitlab.com/ee/topics/build_your_application.html).
Helt kort giver det mulighed for at bygge og teste software i docker containere.  
Gitlab kan også hoste statiske hjemmesider, alle gitlab projekter har mulighed for dette. Det kaldes [Gitlab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).

I denne øvelse kan du lære hvordan du bruger gitlab's CI/CD og gitlab pages til at bygge din personlige portfolio. Du skal bruge en statisk site generator som hedder mkdocs [https://www.mkdocs.org/](https://www.mkdocs.org/)  
Portfolio siden skal du bruge til at vise de ting du lærer på uddannelsen.

Det er også muligt at bruge mkdocs-material der er en udvidelse af mkdocs. Her er stylingen baseret på google material design og der findes en del udvidelser, det er lidt pænere grafisk men valget er dit.  
mkdocs material dokumentationen er her [https://squidfunk.github.io/mkdocs-material/getting-started/](https://squidfunk.github.io/mkdocs-material/getting-started/)  
Et eksempel på opsætning at mkdocs-material kan ses her [https://gitlab.com/ucl-pba-its/fag-template](https://gitlab.com/ucl-pba-its/fag-template)  
Hvis du vælger at bruge mkdocs-material i øvelsen, skal du bruge `requirements.txt` + `.gitlab-ci.yml` + `mkdocs.yml` fra [https://gitlab.com/ucl-pba-its/fag-template](https://gitlab.com/ucl-pba-its/fag-template) istedet for dem herunder.   

mkdocs er en statisk site generator som oversætter markdown filer til HTML. Du behøver altså ikke at vide noget om at skrive HTML, css og javascript for at lave en hjemmeside.

Øvelsen formoder at du arbejder med et gitlab projekt som er klonet lokalt til din computer.

Der er temmelig mange instruktioner i denne øvelse og det kan virke mere uoverskueligt end det egentlig er, derfor har jeg har lavet et eksempel projekt så du har et sted at sammenligne med: [https://gitlab.com/npes/mkdocs-test](https://gitlab.com/npes/mkdocs-test)

Jeg vil også anbefale at du læser hele øvelsen igennem inden du går i gang med at arbejde, det vil hjælpe dig til at skabe et overblik.  

Jeg vil anbefale du arbejder fra en kodeeditor som [VS Code](https://code.visualstudio.com/) eller lignende. Det er en fordel hvis din editor har en indbygget terminal så du ikke skal skifte imellem forskellige vinduer mens du arbejder!  

## Instruktioner

1. Lav et projekt på gitlab.com i dit eget namespace `https://gitlab.com/DIT_BRUGER_NAVN`
2. Klon projektet til din computer med `git clone`, brug ssh til at klone projektet :-)
3. Installer Python og pip, følg mkdocs guiden, stop ved _Installing MKDocs_ [https://www.mkdocs.org/user-guide/installation/](https://www.mkdocs.org/user-guide/installation/)
4. Naviger til dit gitlab projekt, åben en terminal og lav et Python virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment)
5. Aktiver virtual environment [https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment)
6. Følg mkdocs getting started guiden [https://www.mkdocs.org/getting-started/](https://www.mkdocs.org/getting-started/) til og med punktet **Adding pages**
7. Flyt mappen `docs` og filen `mkdocs.yml` til en mappe i dit projekt, navngiv mappen `pages`
8. pip freeze gemmer dine ,virtual environment, package dependencies, så dit projekt senere kan genskabes uden en backup af selve pip pakkerne.  
   Læs om pip freeze [https://pip.pypa.io/en/stable/reference/pip_freeze/](https://pip.pypa.io/en/stable/reference/pip_freeze/) og lav en `requirements.txt` fil ved at køre kommandoen `pip freeze > requirements.txt`  
   Kontroller at `requirements.txt` indeholder dine dependencies, f.eks.  
   
    ```yaml title="requirements.txt" linenums="1"
    text click==7.1.2
    future==0.18.2
    Jinja2==2.11.2
    joblib==0.17.0
    livereload==2.6.3
    lunr==0.5.8 Markdown==3.3.3
    MarkupSafe==1.1.1
    mkdocs==1.1.2
    nltk==3.5
    PyYAML==5.3.1
    regex==2020.11.13
    six==1.15.0
    tornado==6.1
    tqdm==4.53.0
    ```

## Kør siden lokalt i din browser

Når du arbejder med at tilføje indhold er det rart at kunne se ændringerne lokalt i din browser inden de bliver bygget og udgivet på gitlab pages.  
Det sikrer at du ikke har fejl i links og formattering på din "offentlige" side, altså at din side virker og ser ud som du ønsker inden du udgiver den.  

1. Kør siden lokalt med kommandoen `mkdocs serve` fra **pages** mappen.
2. Åbn din browser og naviger til `http://127.0.0.1:8000`

Hvis alt virker bør du se forsiden på mkdocs siden i din browser, hvis ikke så check for fejl i terminalen.

## .gitignore fil

Det er god praksis ikke at gemme dependencies i dit git repository, de fylder unødvendigt og er ikke nødvendige for at genskabe projektet nu hvor du har din `requirements.txt`  
Dependencies i dette projekt er dit virtuelle environment samt evt. mkdocs site mappen.

   1. I roden af din gitlab projekt mappe, lav en fil der hedder `.gitignore`
   2. Åben `.gitignore` i en teksteditor
   3. Tilføj 2 seperate linjer i filen, `env/` og `site/` linjerne fortæller git at disse mapper skal ignoreres

## Gitlab pipeline fil

Gitlab bruger Gitlab runners til at køre docker containers [https://docs.gitlab.com/runner/](https://docs.gitlab.com/runner/)  
Docker containere kører et OS image i en container.  
Gitlab runner pipeline konfigurationen laver du i dit projekt, i en fil der hedder `.gitlab-ci.yml`  
Filens sprog/syntaks hedder [yaml](https://en.wikipedia.org/wiki/YAML) og de kommandoer der eksekveres er `bash` kommandoer.

Du kan læse mere om gitlab runners, pipelines og jobs her [https://docs.gitlab.com/ee/ci/pipelines/](https://docs.gitlab.com/ee/ci/pipelines/)

I dit MKDocs projekt skal du konfigurere en pipeline som bruger en docker container med linux alpine som image.  
I den container installeres et par ting, inklusiv MKDocs og herefter køres kommandoen `mkdocs build`  
Endelig flyttes build outputtet (din mkdocs side) til gitlab projektets public mappe. Gitlab pages filern skal ligge i gitlab projektets public mappe, det er her gitlab server siden fra.

1. I gitlab projektets rod lav en fil der hedder `.gitlab-ci.yml`
2. Tilføj nedenstående til filen:

    ```yaml title=".gitlab-ci.yml" linenums="1"
    image: python:alpine
    before_script:
    - apk add --no-cache --virtual .build-deps gcc musl-dev
    - pip install cython
    - pip install --upgrade pip
    - pip install mkdocs
    # Add your custom theme if not inside a theme_dir
    # (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
    # - pip install mkdocs-material
    pages:
        script:
            - cd pages
            - mkdocs build
            - mv site ../public
        artifacts:
            paths:
            - public
        only:
            - main
    ```

11. git add og commit nye filer

    1. Tilføj nye filer med `git add .`
    2. Commit med `git commit -am "added mkdocs"`
    3. Push med `git push`

12. Check din pipeline

    1. Naviger til projektet på gitlab.com
    2. Klik på `Build` i menuen til venstre
    3. Klik på `Pipelines`
    4. Klik på jobbet for at se hvad der sker
    5. Hvis jobbet er kørt uden fejl vil den sidste linje vise `Job succeeded`, hvis ikke så læs fejlbeskrivelsen og debug.

13. Kontroller at siden kører på gitlab

    1. På gitlab.com, i dit projekt, naviger til Deploy > Pages og klik på linket under `Access pages`
    2. Første gang siden bygges kan der gå op til 30 minutter før siden er "live"


## Links

- MKDocs [https://www.mkdocs.org/](https://www.mkdocs.org/)
- Theme [https://squidfunk.github.io/mkdocs-material/](https://squidfunk.github.io/mkdocs-material/)
- More on Theme [https://squidfunk.github.io/mkdocs-material/](https://squidfunk.github.io/mkdocs-material/)

### Plugins til MKDocs

- Git revision plugin [https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/](https://pypi.org/project/mkdocs-git-revision-date-localized-plugin/)
- linkchecker [https://github.com/manuzhang/mkdocs-htmlproofer-plugin](https://github.com/manuzhang/mkdocs-htmlproofer-plugin)
- PDF builder [https://github.com/brospars/mkdocs-page-pdf](https://github.com/brospars/mkdocs-page-pdf)
