# Øvelse 21 - tilføj interface til vsrx

### Information

Udover de 2 xubuntu maskiner vil vi gerne have en kali linux VM på netværket også. Det betyder at vi mangler et interface på vsrx.  
Det er heldigvis simpelt at tilføje et nyt interface i vmware wworkstation, konfiguration af interfacet kan du jo allerede ;-)

![opnsense_intro_til_itsik1.png](opnsense_intro_til_itsik1.png)  
*Netværksdiagram*

### Instruktioner

1. sluk vsrx (power off vm)
2. åbn settings og tilføj en netværksadapter
3. åbn indstillinger og forbind network adapter 4 (ge-0/0/3) til vmnet3 (hvis du ikke har vmnet3 så lav det og konfigurer det)
4. boot router, log ind, start junos cli
5. skriv `show interfaces terse` for at se at du nu har ge-0/0/3
6. gå i edit mode `edit`
7. `edit interfaces`
8. konfigurer ge-0/0/3 med `set ge-0/0/3 unit 0 family inet address 192.168.12.1/24`
9. gem ændringer med `commit`
10. `show route terse` for at se routing tabel
11. `top` for at komme ud af edit interfaces
12. `edit security zones`
13. inkluder ge-0/0/3 i trust zonen med `set security-zone trust interfaces ge-0/0/3 host-inbound-traffic system-services ping`
14. gem ændringer `commit`
15. `show` for at se security zones config
16. ping router interfacet fra en af dine xubuntu maskiner for at bekræfte at dit netværk virker

## Links
