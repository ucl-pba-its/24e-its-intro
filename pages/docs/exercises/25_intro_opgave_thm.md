# Øvelse 25 - TryHackMe

## Information

TryHackMe (THM) er en populær web platform til at træne mange forskellige IT sikkerheds relaterede evner.
Det er muligt, via VPN forbindelse at forbinde til THM's netværk og på den måde bruge egen maskine til THM rummene.
Det giver lidt mere frihed at bruge sin egen maskine på THM, alternativet er det THM kalder en attackbox men der er en daglig begrænsning hvis du ikke betaler for THM.
Udover det er det lettere at have notater og opsætning på egen maskine, på attackboxen forsvinder de når den slukkes.

Det anbefales at bruge Kali Linux i en virtuel maskine til rummene på THM.

## Instruktioner

1. Opret en konto på [https://tryhackme.com/](https://tryhackme.com/) - det anbefales at bruge din skole email fordi du kan få rabat [https://help.tryhackme.com/en/articles/6494960-student-discount](https://help.tryhackme.com/en/articles/6494960-student-discount).
2. Start din Kali Linux VM, hvis du ikke har en så følg _Øvelse 24 - Kali Linux virtuel maskine i Virtualbox_
3. Følg guiden fra THM (kræver at du er logget på THM) [https://tryhackme.com/access?o=vpn](https://tryhackme.com/access?o=vpn)
4. Gennefør rummet _Tutorial_ på THM [https://tryhackme.com/room/tutorial](https://tryhackme.com/room/tutorial) - Husk at du IKKE skal bruge _Attackbox_ fordi du er forbundet via VPN.

Note! Der kan være problemer med at forbinde til THM via vpn (OpenVPN, port 1194) gennem skolens netværk (eduroam), hvis du har problemer med at forbinde så prøv et andet netværk, f.eks et hotspot på din telefon.

## Links
