# Øvelse 2 - Git/gitlab og ssh nøgler

## Information

I denne øvelse skal du opsætte git på din computer, lave ssh nøgler og forbinde til gitlab (og andre steder) med ssh.
Det hele er beskrevet i en guide som nok kræver at du læser lidt dokumentation forskellige steder. Guiden linker til de ressourcer du har brug for.  
Ud over det skal du også opsætte en user agent så du ikke behøver taste dit ssh key password hver gang du laver f.eks en git push.

SSH nøglepar består af en offentlig og en privat del. Den private nøgle har ikke nogen fil type og den offentlige har filtypen `.pub`

Du bør aldrig dele din private nøgle eller have den liggende andre steder end på din private maskine. Eneste undtagelse er en backup af din private nøgle, her vil jeg overlade det til dig at finde et egnet sted, en usb stick, en password manager eller andet.

Din offentlige SSH nøgle kan, som navnet antyder, deles med ofentligheden. For at tilgå andre maskiner eller services med SSH skal din offentlige nøgle være placeret på det sted du forsøger at tilgå.  
Det kan være lidt forskelligt hvor du placerer den når du bruger services som f.eks gitlab, på en remote maskine skal den ligge i `~/.ssh/authorized_keys` filen.

## Instruktioner

**Del 1 - git, gitlab og ssh**  

Følg denne guide for at opsætte git, gitlab og ssh nøgler [https://eal-itt.gitlab.io/gitlab_daily_workflow/index.html#0](https://eal-itt.gitlab.io/gitlab_daily_workflow/index.html#0) sørg for at klikke på alle links for at få så meget viden som muligt

**Del 2 - ssh user agent**

Som sagt i beskrivelsen til denne øvelse kan du tilgå maskiner og services med ssh hvis du har tilføjet en offentlig ssh nøgle som matcher din private ssh nøgle.  
Det virker fint, men hvad hvis du har brug for at tilgå en 3. maskine, med ssh, fra den 2. maskine?  
Det vil ikke virke med mindre din private ssh nøgle er på den 2. maskine, men det er som sagt ikke en god ide at have din private nøgle andre steder end din private maskine.  
En løsning er at opsætte en _ssh user agent_ og tilføje din private nøgle til agenten. Agenten kan så _forwardes_ til en remote maskine og dermed gøre det muligt at tilgå yderlige maskiner fra den 2. maskine.  
Nå du afslutter din ssh session vil agenten trækkes tilbage og sørge for at din private ssh nøgle ikke er på andre maskiner mere.

I denne øvelse kommer du ikke længere end fordelen ved at have en user agent og slippe for at skrive dit password igen og igen. Med mindre du har en remote maskine kan du også prøve at forwarde din user agent til den maskine.

**Del 3 - opsæt ssh user agent og autostart**

1.  Åben git bash (eller anden terminal der kan køre git bash)
2.  Start din user agent `eval $(ssh-agent -s)`
3.  Tilføj din ssh nøgle til agenten `ssh-add ~/.ssh/id_ed25519`
4.  Opsæt user agent autostart ved at følge denne guide [https://docs.github.com/en/authentication/connecting-to-github-with-ssh/working-with-ssh-key-passphrases#auto-launching-ssh-agent-on-git-for-windows](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/working-with-ssh-key-passphrases#auto-launching-ssh-agent-on-git-for-windows)  
    Hvis du ikke har en `.bashrc` fil, så opret den i roden af din brugers home mappe `~/`

_Hvis du ikke opsætter autostart er du nød til manuelt at starte din agent hver gang din maskine genstartes med kommandoen `eval $(ssh-agent -s)`_

**Del 4 - forward ssh user agent**

1. Sørg for at din ssh user agent kører, hvis du har fulgt **del 3** så burde din user agent starte når du åbner din terminal, hvis ikke så gå tilbage til **del 3** og få det til at virke.
2. Åben en terminal og ssh til din remote maskine med syntaksen `ssh -A user@myhost.com`  
    myhost.com kan selvfølgelig også være en ip adresse. Brugeren er den bruger du har opsat på din remote maskine.  
    `-A` parametret fortæller ssh at agenten skal forwardes.  
   Du kan læse mere om ssh kommando parametre på [https://man.openbsd.org/ssh](https://man.openbsd.org/ssh) eller ved at skrive `man ssh` hvis du er på linux.
3. Fra din remote maskine test at din private nøgle er forwardet ved at verificere med gitlab.com, kør kommandoen `ssh -T git@gitlab.com`

## Links
