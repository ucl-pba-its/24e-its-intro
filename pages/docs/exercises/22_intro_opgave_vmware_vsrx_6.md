# Øvelse 22 - Kali linux vm på netværket

### Information

På dit virtuelle netværk skal du nu opsætte en kali linux maskine der kan bruges som _attack box_

![opnsense_intro_til_itsik1.png](opnsense_intro_til_itsik1.png) 
*Netværksdiagram*

### Instruktioner

1. Installer en kali linux vm, brug den du har i forvejen, eller klon din eksisterende (vælg fuld klon)
2. sæt kali maskinens VM network adaptor til vmnet3
3. sæt maskinens ip til `192.168.12.2` netmask `255.255.255.0` gateway `192.168.12.1`
4. ping gateway og de 2 xubuntu maskiner for at bekræfte at dit netværk virker

## Links
