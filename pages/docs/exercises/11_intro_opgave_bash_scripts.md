# Øvelse 11 - Forstå bash script + tilrette script

## Information

Det er en forudsætning for denne øvelse at du har en virtuel linux maskine (Linux VM) med nmap installeret.

Nmap kan findes her: [https://nmap.org/](https://nmap.org/)
og installeres ved først at opdatere apt package list med `sudo apt-get update` og derefter installere nmap med `sudo apt-get install nmap`

Check om nmap er installeret med `nmap --version`

## Instruktioner

1. Diskuter nedenstående script med dit team. Hvad sker der på hver linje i dette script? – I skal snakke det igennem FØR I kører koden. Ideen er at forstå koden.
2. Implementer dette script i din linux VM. Når du har fået det til at virke, så tilret således nmap resultatet ikke skrives til en fil, men at alle de åbne porte printes direkte i konsolvinduet (uden anden output).
  
    ![script](nmapscans_example.png)

## Links

- Dette cheat sheet [https://devhints.io/bash](https://devhints.io/bash) kan bruges mens du arbejder med øvelsen.
