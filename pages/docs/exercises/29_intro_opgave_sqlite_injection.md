# Øvelse 29 - SQL injection i sqlite

## Information

I denne øvelse skal i prøve at lave SQL injection i Chinook databasen.  
Målet er at lække oplysninger fra andre tabeller end den der er intenderet i applikationen.  
Den type SQL injection i skal lave er en UNION baseret injection.  

Øvelsen er en gruppe øvelse hvor i arbejder individuelt men besvarer øvelsens spørgsmål sammen.  
Jeres svar vender vi på klassen sammen efter øvelsen.  

Inden i laver denne øvelse kan det hjælpe at have lavet [øvelse 9](../exercises/9_intro_opgave_python_db.md)

## Instruktioner

1. Læs om [insecure SQL injection i sqlite3](https://docs.python.org/3/library/sqlite3.html#how-to-use-placeholders-to-bind-values-in-sql-queries)  
    Læg mærke til at det er usikkert at tillade rå SQL kommandoer fra bruger input, i stedet bør i bruge parametriserede værdier.  
    Læg også mærke til hvad forskellen er på de to så i ved hvad i skal undgå i dine fremtidige applikationer.  
2. Klon dette gitlab projekt: [https://gitlab.com/npes-py-experiments/database-programs](https://gitlab.com/npes-py-experiments/database-programs) 
3. Start applikationen `chinook_insecure.py` (Spoiler, applikationen er usikker)  
    **! ! I må ikke kigge på koden endnu ! !**
4. Prøv at hente data fra databasen ved at skrive et tal som input.  

    **Spørgsmål:** 

    - **Hvilken data får i ud?**   
    - **Hvad tror i tabellen der hentes data fra hedder? Hvordan kan i finde ud af det**  
    - **Hvor mange rækker er der i tabellen? (prøv med høje tal)**  
    - **Kan i få databasen til at fejle?**  

Nu hvor i har dannet jer et overblik over hvordan applikationen fungerer og hvilken data i kan hente er det tid til at afprøve og (forhåbentlig) forstå hvordan i kan bruge SQL Union injection til at lække data fra databasen.

1. Hvilken data returneres når i prøver at skrive `''` som input?   
    I får et tom resultat? Hvorfor mon det?? Det er fordi i nu bryder applikationens SQL kommando, læg mærke til at i ikke får en database fejl!
2. Hvilken data returneres når i skriver `'' OR TRUE; --` ?   
    _Interessant!_ Forespørgslen evaluerer altid til sandt og så får i pludselig en del data!! (hvis ikke gør i noget galt)
3. Det er jo meget fint at alt data i tabellen lækkes på en gang, men det afslører ikke rigtig noget i ikke kunne tilgå alligevel.  
    Det næste skridt er at finde ud af hvor mange kolonner der er i tabellen med:  
    `1 ORDER BY 1` derefter `1 ORDER BY 1,2` derefter `1 ORDER BY 1,2,3` osv. indtil i får en fejl, når i får en fejl er det fordi i har for mange kolonner i din SQL statement.  
    Grunden til at i skal kende antallet af kolonner ser i når i kommer til næste punkt....  

    **Spørgsmål: Hvor mange kolonner har tabellen?**  

4.  Nu hvor i kender antallet af kolonner kan i lække hvilke tabeller der er i databasen med:  

    `1 UNION SELECT 1,group_concat(tbl_name),3 FROM sqlite_master WHERE type='table' and tbl_name NOT like 'sqlite_%'`  
    
    [SQL UNION](https://www.w3schools.com/sql/sql_union.asp) kommandoen kombinerer flere select statements, så det er muligt at lave forespørgsler, der ikke er en del af applikationens intendere funktionalitet.  
    Den SQL kommando der afvikles i applikationen ser således ud:  

    `SELECT * FROM albums WHERE AlbumId = '' UNION SELECT 1,group_concat(tbl_name),3 FROM sqlite_master WHERE type='table' and tbl_name NOT like 'sqlite_%'`  
    
    Læg mærke til hvordan kommandoen forbigår den oprindelige forespørgsel `SELECT * FROM albums WHERE AlbumId = ''` der jo returnerer et tomt resultat som i lærte i punkt 1 og tilføjer endnu en SELECT, som i kan kontrollere uafhængigt af applikationen    !  

5. I kender nu databasens tabeller. En trusselsaktør vil nok gå efter brugernavne, email adresser, passwords og anden sensitiv information der kan bruges til at få adgang til andre dele af virksomhedens systemer, forsøg på phishing etc..  
    Databasen har nogle medarbejdere i tabellen `employees`, gad vide om der er noget spændende i den tabel?  
    Denne kommando lækker hvordan tabellen er oprettet og afslører hvilke kolonner der er i databasen, deres datatyper osv: 

    `1 union SELECT 1,sql,3 FROM sqlite_master WHERE type!='meta' AND sql NOT NULL AND name NOT LIKE 'sqlite_%' AND name ='table_name'`  
    
    I skal udskifte `table_name` i kommandoen med den tabel i vil bruge, f.eks employees (men i kan egentlig vælge den tabel i har lyst til.)   
    
    Eksemplet her bruger employees tabellen og kolonnen `Email`.  
    
    **Spørgsmål: Hvilken kolonne har i lyst til at få data fra i jeres valgte tabel??**    

6. Nu er det simpelt at lække data med kommandoen:  

    `2 UNION SELECT 1,col_name,3 FROM table_name`  
    
    husk at erstatte `col_name` og `table_name` med jeres valgte, tabel og kolonne navne.
    
    **Spørgsmål:** 
    
    - **Hvor mange resultater får i?**  
    - **Kan i afgøre noget om hvilken måde virksomheden laver bruger navne f.eks _initialer_ eller _fornavn.efternavn_?**  

7. Nu må i gerne kigge i koden :-)

**Spørgsmål:**  

- **Hvordan tror i at data fra databasen kan misbruges? (Kan CIA bruges her? Er data penge værd?)**  
- **Hvad er meget vigtigt at i ikke tillader når i bruger databaser i applikationer?**  
- **Hvad skal i anvende i sqlite så det ikke er muligt at lave injections?**  

**BONUS** Kig i koden og omskriv den så applikationen bruger parametriseret input og test at den ikke kan kompromitteres mere. 

## Links

- _chinook_ databasens struktur [https://www.sqlitetutorial.net/sqlite-sample-database/](https://www.sqlitetutorial.net/sqlite-sample-database/)
- _chinook_ databasens ER diagram [https://www.sqlitetutorial.net/wp-content/uploads/2018/03/sqlite-sample-database-diagram-color.pdf](https://www.sqlitetutorial.net/wp-content/uploads/2018/03/sqlite-sample-database-diagram-color.pdf)