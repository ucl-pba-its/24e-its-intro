# Øvelse 5 - Nyhedskanaler

## Information

Næsten dagligt er der nyheder om it sikkerheds branchen. Det kan være nyheder om nye botnet, 0day, sikkerheds forskning etc.

Men hvor kan disse nyheder findes og hvad er den bedste måde at modtage dem ?

Formålet med denne øvelse er at få jer til at følge med i branchens nyheder. Vi kommer til at starte hver undervisningsgang i faget med at snakke om hvad der rører sig i it sikkerheds branchen, selvfølgelig baseret på hvad i synes der er relevant ud fra de nyheder i læser igennem ugen.

## Instruktioner

1. Research kilder til nyheder om it sikkerhed. Lav en liste der for hver kilde indeholder:
   - Web adresse
   - Type af nyheder
   - Hvem står bag siden ?
   - Hvordan kan du tilgå nyhederne (RSS, mail, api, manuelt besøg, andet?)
2. Gå sammen med dine medstuderende og lav et samlet markdown dokument der har ovenstående oplysninger. Hvordan kan i dele dokumentet så alle har adgang?
3. Bliv enige om hvordan og hvor ofte det er mest hensigtsmæssigt at tilgå nyhederne, er det muligt f.eks at lave en applikation der scraper nyheder fra alle jeres kilder ?

## Links
