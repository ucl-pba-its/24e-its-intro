# Øvelse 24 - Kali Linux virtuel maskine i Virtualbox

## Information

Denne øvelse handler om at installere virtualbox og derefter installere Kali Linux i en virtuel maskine.
Virtualbox er en hypervisor som gør det muligt at køre virtuelle maskiner på din computer.
Kali Linux er en Linux distribution målerettet IT sikkerhed og har et stort katalog af værktøjer som bruges af IT sikkerheds professionelle.  

## Instruktioner

1. Download Virtualbox [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
2. Installer Virtualbox [https://www.virtualbox.org/manual/UserManual.html#intro-installing](https://www.virtualbox.org/manual/UserManual.html#intro-installing)
3. Download Kali Linux til Virtualbox [https://www.kali.org/get-kali/#kali-virtual-machines](https://www.kali.org/get-kali/#kali-virtual-machines)
4. Importer Kali Linux ved hjælp af denne guide [https://www.kali.org/docs/virtualization/import-premade-virtualbox/](https://www.kali.org/docs/virtualization/import-premade-virtualbox/)
5. Start Kali Linux VM og log ind, Default credentials er **kali:kali** (user:pass)
6. Åbn en terminal og ping `8.8.8.8` for at sikre at du har forbindelse til internettet

## Links

- Kali Linux dokumentation [https://www.kali.org/docs/](https://www.kali.org/docs/)
- Virtualbox manual [https://www.virtualbox.org/manual/UserManual.html](https://www.virtualbox.org/manual/UserManual.html)