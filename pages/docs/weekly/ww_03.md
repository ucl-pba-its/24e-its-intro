---
Week: 8
Content: xx, xx, xx, xx
Material: See links in weekly plan
Initials: NISI
---

# Uge 38 - Sikkerhed i netværksprotokoller

## Mål for ugen

Denne uge omhandler netværk og sikkerhed i netværk.
Emnerne belyses ved dels at arbejde med Wireshark for at lære at analysere mistænkelig netværkstrafik, dels ved at opsætte et lille netværk i virtualbox.
virtualbox netværket udvides i næste uge med installation af en sårbar webapplikation. Målet med netværket er at den studerende har et lukket miljø til at træne i værktøjer der kan bruges i sikkerheds arbejde.
Endelig er der en Tryhackme øvelse der introducerer netværks scannings værktøjet NMAP.

Der er en del arbejde i dagens øvelser og det er derfor vigtigt at i bruger alt den tid i har til rådighed i dag.

### Forberedelse

- Læs undervisningsplanen og øvelser
- Læs kapitel 4 – Beskyttelse af it-systemer, i "IT-Sikkerhed i praksis", hav fokus på netværk.
- Se CTF video hvor wireshark bruges [https://youtu.be/A4_DOr7Eiqo](https://youtu.be/A4_DOr7Eiqo)
- Se video om hvordan netværkspakker bevæger sig på et netværk [How Packets Move Through a Network](https://youtu.be/rYodcvhh7b8?feature=shared)

### Praktiske mål

- lubuntu maskiner oprettet og konfigureret i virtualbox til at bruge opnsense
- opnsense virtuel router konfigureret i virtualbox

### Øvelser

2. [Øvelse 16 - Opsætning af virtuelle maskiner i virtualbox](../exercises/16_intro_opgave_vmware_lubuntu.md)
3. [Øvelse 17 - Opsætning af opnsense virtuel router](../exercises/17_intro_opgave_vbox_opnsense_1.md)
4. [Øvelse 18 - Udforskning af opnsense cli og webinterface](../exercises/18_intro_opgave_vbox_opnsense_2.md)
8. [THM: Wireshark - the basics](https://tryhackme.com/room/wiresharkthebasics)
1. [Øvelse 26 - Wireshark analyse](../exercises/26_intro_wireshark_analyse.md)
7. [THM: NMAP](https://tryhackme.com/room/furthernmap)

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for:_** 
    - Grundlæggende netværksprotokoller
    - Sikkerhedsniveau i de mest anvendte netværksprotokoller

- **_Den studerende kan supportere løsning af sikkerhedsarbejde ved at:_**  
    - Opsætte et simpelt netværk.
    - Mestre forskellige netværksanalyse tools


#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kan anvende nmap til at skanne et netværk og wireshark til at sniffe netværkstrafik
- Den studerende kan i et virtuelt miljø (virtualbox) opsætte netværksenheder (opnsense og virtuelt netværk) og konfigurere netværk på Linux hosts (lubuntu, kali linux)

## Afleveringer

- De praktiske mål er en forudsætning for at kunne arbejde med næste uges øvelser.
- Øvelser dokumenteret på gitlab pages

## Skema

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Øvelser fra sidst      |
| 9:30  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer og links


