---
Week: 6
Content: GIT, Gitlab, Jobs, Nyheder
Material: See links in weekly plan
Initials: NISI
---

# Uge 36 - Introduktion til faget og opsætning af værktøjer

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Forberedelse

- Læs undervisningsplanen og Øvelser
- Læs kapitel 1 – it-kriminalitet og trusler fra cyberspace, i “IT Sikkerhed i praksis” (se [boglisten](https://esdhweb.ucl.dk/D24-2618511.pdf))

### Praktiske mål

- Spørgeskema om forudsætninger udfyldt
- Virtualbox installeret
- Kali Linux VM installeret i virtualbox
- Alle studerende har forbindelse til TryHackMe via Kali/VPN
- THM Linux fundamentals part 1+2+3 gennemført

### Øvelser

1. [Spørgeskema om forudsætninger](https://forms.office.com/e/JrrVw2CEQZ)
1. [Øvelse 4 - Studieordning og læringsmål](../exercises/4_intro_opgave_læringsmål.md)
2. [Øvelse 24 - Kali Linux virtuel maskine i Virtualbox](../exercises/24_intro_opgave_virtualbox_kali.md)
3. [Øvelse 25 - TryHackMe](../exercises/25_intro_opgave_thm.md)
4. [THM: Learning Cyber Security](https://tryhackme.com/room/beginnerpathintro)
5. [THM: Linux fundamentals part 1](https://tryhackme.com/room/linuxfundamentalspart1)
5. [THM: Linux fundamentals part 2](https://tryhackme.com/room/linuxfundamentalspart2)
5. [THM: Linux fundamentals part 3](https://tryhackme.com/room/linuxfundamentalspart3)

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for:_** 
    - Grundlæggende netværksprotokoller
    - Sikkerhedsniveau i de mest anvendte netværksprotokoller

- **_Den studerende kan supportere løsning af sikkerhedsarbejde ved at:_**  
    - Opsætte et simpelt netværk 

- **_Den studerende kan:_** 
    - ..

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kender fagets læringsmål og fagets semester indhold
- Den studerende kan vurdere egne faglige evner i relation til fagets læringsmål
- Den studerende har viden om virtuelle maskiner
- Den studerende har viden om VPN opsætning

## Afleveringer

- Spørgeskema om forudsætninger

## Skema

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer og links


