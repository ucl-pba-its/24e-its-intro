---
Week: 16
Content: xx, xx, xx, xx
Material: See links in weekly plan
Initials: NISI
---

# Uge 45 - Eksamen repetition og forberedelse

Repetitionen består af øvelser i kan lave selvstændigt.

Øvelse 52 skal du bruge til at danne dig et overblik over hvad du mangler at læse op på ifht. fagets læringsmål.

Øvelse 51 skal du bruge til at lave en plan for forberedelse af præsentationer og evt. demoer du skal bruge til eksamen.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Forberedelse

- Læs undervisningsplanen og øvelser

### Praktiske mål

- Liste der skaber overblik over, hvad du mangler at læse op på og hvilke øvelser du mangler at lave, inden eksamen - Øvelse 52
- Præsentationer til brug ved eksamen - Øvelse 51

### Øvelser

1. [Øvelse 52 - Repetition af faget](../exercises/52_intro_opgave_eksamen_repetition.md)
2. [Øvelse 51 - Eksamens forberedelse](../exercises/51_intro_opgave_eksamen_spørgsmål.md)

### Læringsmål der arbejdes med i faget denne uge

- Alle fagets læringsmål 

## Afleveringer

- Øvelser dokumenteret på gitlab pages

## Skema

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Øvelser fra sidst      |
| 9:30  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer og links


